# Raspberry Pi 4 Configuration

**Disclaimer**
Offered with no support or warranty.

**Introduction**
Added this repository to share my Raspberry Pi 4 configuration to resolve
sharing multiple monitors and resolve no sound issues.

**Instructions**
All changes should be made to /boot/config.txt
or /boot/firmware/usercfg.txt in Ubuntu to resolve.

**Multiple Monitors**
Add the following:
`disable_overscan=1`


**Audio Issues**
The HDMI monitors that I had purchased were either throwing incorrect CEA codes
back to the Pi or the Pi was interpreting the CEA codes incorrectly.

This meant that all that could be heard were very faint clicks. This should be
resolveable by getting the CEA file, hacking a few bites and instructing the
firmware to load. This however, was outside of my abilities and also not
required as I did not require the HDMI audio support.

To resolve, enable the audio with the addition of the following:
`dtparam=audio=on`

Instruct the Raspberry Pi to ignore the HDMI instructions and pass the audio
through the 3.5mm audio jack by adding the following code:
`hdmi_ignore_edid_audio=1`

On boot, open the terminal and instruct ALSA to use the 3.5mm audio and not
the HDMI interface by running:
`sudo amixer cset numid=2 1`
`sudo amixer cset numid=3 1`

Congratulations, you should no have multiple monitors via the HDMI ports and
audio coming from the 3.5mm audio jack.

I've added my personal configuration files to resolve. Hope this helps.
